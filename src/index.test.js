jest.mock('react-dom', () => ({
    render: jest.fn(),
}));

import {createMessage} from './index';

describe('createMessage', () => {
    /*
    test('nameが未入力の場合', () => {
        expect(createMessage('')).toBe('');
    });
    */
    test('nameが入力されている場合', () => {
        expect(createMessage('abc')).toBe('こんにちは、abcさん');
    });
});