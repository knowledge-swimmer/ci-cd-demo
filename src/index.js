import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

export function createMessage(name) {
  if (name) {
    return `こんにちは、${name}さん`;
  } else {
    return '';
  }
}


function Message(props) {
  return <div>{createMessage(props.name)}</div>;
}

function Greeting() {
    const [text, setText] = useState('');
    const [name, setName] = useState('');

    return (
      <div>
        <div>
          お名前を入力してください: <input onChange={(e) => setText(e.target.value)}/>
          <button onClick={() => setName(text)}>OK</button>
        </div>
        <Message name={name} />
      </div>
    );
}


ReactDOM.render(<Greeting />, document.getElementById('root'));



function dummyFunction1(name) {
  if(name === 'a') {
    return 1;
  } else if(name === 'b') {
    return 2;
  } else if(name === 'c') {
    return 3;
  } else if(name === 'd') {
    return 4;
  } else if(name === 'e') {
    return 5;
  } else if(name === 'f') {
    return 6;
  } else if(name === 'g') {
    return 7;
  } else if(name === 'h') {
    return 8;
  } else if(name === 'i') {
    return 9;
  } else if(name === 'j') {
    return 10;
  } else if(name === 'k') {
    return 11;
  } else if(name === 'l') {
    return 12;
  } else if(name === 'm') {
    return 13;
  } else if(name === 'n') {
    return 14;
  } else if(name === 'o') {
    return 15;
  } else if(name === 'p') {
    return 16;
  } else if(name === 'q') {
    return 17;
  } else if(name === 'r') {
    return 18;
  } else if(name === 's') {
    return 19;
  } else if(name === 't') {
    return 20;
  } else if(name === 'u') {
    return 21;
  } else if(name === 'v') {
    return 22;
  } else if(name === 'x') {
    return 23;
  } else if(name === 'y') {
    return 24;
  } else if(name === 'z') {
    return 25;
  }
  return name;
}

function dummyFunction2(name) {
  if(name === 'a') {
    return 1;
  } else if(name === 'b') {
    return 2;
  } else if(name === 'c') {
    return 3;
  } else if(name === 'd') {
    return 4;
  } else if(name === 'e') {
    return 5;
  } else if(name === 'f') {
    return 6;
  } else if(name === 'g') {
    return 7;
  } else if(name === 'h') {
    return 8;
  } else if(name === 'i') {
    return 9;
  } else if(name === 'j') {
    return 10;
  } else if(name === 'k') {
    return 11;
  } else if(name === 'l') {
    return 12;
  } else if(name === 'm') {
    return 13;
  } else if(name === 'n') {
    return 14;
  } else if(name === 'o') {
    return 15;
  } else if(name === 'p') {
    return 16;
  } else if(name === 'q') {
    return 17;
  } else if(name === 'r') {
    return 18;
  } else if(name === 's') {
    return 19;
  } else if(name === 't') {
    return 20;
  } else if(name === 'u') {
    return 21;
  } else if(name === 'v') {
    return 22;
  } else if(name === 'x') {
    return 23;
  } else if(name === 'y') {
    return 24;
  } else if(name === 'z') {
    return 25;
  }
  return name;
}

console.log(dummyFunction1());
console.log(dummyFunction2());
